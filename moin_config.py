# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - Configuration

    Note that there are more config options than you'll find in
    the version of this file that is installed by default; see
    the module MoinMoin.config for a full list of names and their
    default values.

    Also, the URL http://purl.net/wiki/moin/HelpOnConfiguration has
    a list of config options.

    @copyright: 2000-2003 by J�rgen Hermann <jh@web.de>
    @license: GNU GPL, see COPYING for details.
"""
# If you run several wikis on one host (commonly called a wiki farm),
# uncommenting the following allows you to load global settings for
# all your wikis. You will then have to create "farm_config.py" in
# the MoinMoin package directory.
#
# from MoinMoin.farm_config import *

# basic options (you normally need to change these)
sitename = 'GNOME Live!'
interwikiname = None
data_dir = '/usr/local/www/live.gnome.org/data'
url_prefix = '/moinmoin/'
logo_url = url_prefix + '/classic/img/moinmoin.png'

# encoding and WikiName char sets
# (change only for outside America or Western Europe)
charset = 'UTF-8'
upperletters = "A-Z������������������������������"
lowerletters = "0-9a-z���������������������������������"

# options people are likely to change due to personal taste
show_hosts = 1                          # show hostnames?
nonexist_qm = 1                         # show '?' for nonexistent?
backtick_meta = 0                       # allow `inline typewriter`?
allow_extended_names = 1                # allow ["..."] markup?
edit_rows = 20                          # editor size
max_macro_size = 50                     # max size of RecentChanges in KB (0=unlimited)
bang_meta = 1                           # use ! to escape WikiNames?
show_section_numbers = 0                # enumerate headlines?

# charting needs "gdchart" installed!
# you can remove the test and gain a little speed (i.e. keep only
# the chart_options assignment, or remove this code section altogether)
try:
    import gdchart
    chart_options = {'width': 720, 'height': 400}
except ImportError:
    pass

# values that depend on above configuration settings
page_credits = ''
logo_string = ''
html_head = """
<link rel="icon" type="image/png" href="http://www.gnome.org/img/logo/foot-16.png" />
"""

# security critical actions (deactivated by default)
#if 0:
allowed_actions = ['DeletePage', 'AttachFile', 'RenamePage']

# for standalone server (see cgi-bin/moin.py)
#httpd_host = "localhost"
#httpd_port = 80
#httpd_user = "nobody"
#httpd_docs = "/usr/share/moin/wiki/htdocs/"

acl_enabled = 1
acl_rights_default = "Known:read,write,revert,delete,admin All:read"

mail_from = "wiki@gnome.org"
mail_smarthost = "mail.gnome.org"

theme_default = 'gnomey'
theme_force = True
