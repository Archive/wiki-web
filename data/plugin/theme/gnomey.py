# -*- coding: iso-8859-1 -*-
"""
    MoinMoin gnomey theme.

    @copyright: 2003 by ThomasWaldmann (LinuxWiki:ThomasWaldmann)
    @ modified 2004 by Roger Haase (deleted much code, enough to override css remains).
    @license: GNU GPL, see COPYING for details.
"""

from MoinMoin import config
from MoinMoin.theme.classic import Theme as ThemeBase

class Theme(ThemeBase):
    """ This is the gnomey theme. """

    name = 'gnomey'

    stylesheets = ThemeBase.stylesheets + (
        # theme charset         media       basename
        (name,  'utf-8',   'screen',   'gnomey'),
    )
    
    icons = {
        # key         alt                        filename of icon    w   h   
        # -------------------------------------------------------------------
        # navibar
        'help':       ("%(page_help_contents)s", "moin-help.png",   16, 16),
        'find':       ("%(page_find_page)s",     "moin-find.png",   16, 16),
        'diff':       ("Diffs",                  "moin-diff.png",   16, 16),
        'info':       ("Info",                   "moin-info.png",   16, 16),
        'edit':       ("Edit",                   "moin-edit.png",   16, 16),
        'unsubscribe':("Unsubscribe",            "moin-unsubscribe.png",  16, 16),
        'subscribe':  ("Subscribe",              "moin-subscribe.png",16, 16),
        # XXX FIXME: the RAW icon is still missing, so we use XML ...
        'raw':        ("Raw",                    "moin-xml.png",    16, 16),
        'xml':        ("XML",                    "moin-xml.png",    16, 16),
        'print':      ("Print",                  "moin-print.png",  16, 16),
        'view':       ("View",                   "moin-show.png",   16, 16),
        'home':       ("Home",                   "moin-home.png",   16, 16),
        'up':         ("Up",                     "moin-parent.png", 16, 16),
        # FileAttach
        'attach':     ("%(attach_count)s",       "moin-attach.png",  16, 16),
        # RecentChanges
        'rss':        ("[RSS]",                  "moin-rss.png",    36, 14),
        'deleted':    ("[DELETED]",              "moin-deleted.png",60, 12),
        'updated':    ("[UPDATED]",              "moin-updated.png",60, 12),
        'new':        ("[NEW]",                  "moin-new.png",    31, 12),
        'diffrc':     ("[DIFF]",                 "moin-diff.png",   16, 16),
        # General
        'bottom':     ("[BOTTOM]",               "moin-bottom.png", 16, 16),
        'top':        ("[TOP]",                  "moin-top.png",    16, 16),
        'www':        ("[WWW]",                  "moin-www.png",    16, 16),
        'mailto':     ("[MAILTO]",               "moin-email.png",  16, 16),
        'news':       ("[NEWS]",                 "moin-news.png",   16, 16),
        'telnet':     ("[TELNET]",               "moin-telnet.png", 16, 16),
        'ftp':        ("[FTP]",                  "moin-ftp.png",    16, 16),
        'file':       ("[FILE]",                 "moin-ftp.png",    16, 16),
        # search forms
        'searchbutton': ("[?]",                  "moin-search.png", 16, 16),
        'interwiki':  ("[%(wikitag)s]",          "moin-inter.png",  16, 16),
    }

    def header(self, d):
        """
        Assemble page header
        
        @param d: parameter dictionary
        @rtype: string
        @return: page header html
        """
        dict = {
            'config_header1_html': self.emit_custom_html(config.page_header1),
            'config_header2_html': self.emit_custom_html(config.page_header2),
            'logo_html':  self.logo(d),
            'title_html':  self.title(d),
            'username_html': self.username(d),
            'navibar_html': self.navibar(d),
            'iconbar_html': self.iconbar(d),
            'msg_html': self.msg(d),
            'trail_html': self.trail(d),
        }
        dict.update(d)

        html = """
<div id="hdr">
%(config_header1_html)s
%(logo_html)s
%(title_html)s
%(config_header2_html)s
</div>
%(iconbar_html)s
%(username_html)s
%(navibar_html)s
%(trail_html)s
%(msg_html)s
<div id="body">
""" % dict

	return html

    def footer(self, d, **keywords):
        """
        Assemble page footer
        
        @param d: parameter dictionary
        @keyword ...:...
        @rtype: string
        @return: page footer html
        """
        dict = {
            'config_page_footer1_html': self.emit_custom_html(config.page_footer1),
            'config_page_footer2_html': self.emit_custom_html(config.page_footer2),
            'showtext_html': '',
            'edittext_html': self.edittext_link(d),
            'search_form_html': self.searchform(d),
            'available_actions_html': self.availableactions(d),
            'credits_html': self.emit_custom_html(config.page_credits),
            'version_html': '',
            'footer_fragments_html': self.footer_fragments(d),
        }
        dict.update(d)
        
        html = """
</div>
<div id="footer">
<div id="credits">
%(credits_html)s
</div>
%(config_page_footer1_html)s
%(showtext_html)s
%(footer_fragments_html)s
%(search_form_html)s
%(edittext_html)s
%(available_actions_html)s
%(config_page_footer2_html)s
</div>
%(version_html)s
""" % dict

	return html


def execute(request):
    return Theme(request)
